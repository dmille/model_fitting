fprintf('Generating random points with following constants:\n');
a2_1_1;
pause();

fprintf('\nEstimating constants...\n');
a2_1_2;
pause();

fprintf('\nAbsolute error:\n');
a2_1_3;
pause();

fprintf('\nAffine transformation:\n');
a2_2_A;
pause();

fprintf('\nHomography:\n');
a2_2_B;