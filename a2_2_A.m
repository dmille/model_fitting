%% Preprocessing
im1_original = imread('parliament-left.jpg');
im2_original = imread('parliament-right.jpg');
im1 = single(rgb2gray(im1_original));
im2 = single(rgb2gray(im2_original));

%% Resize images for faster processing
scalefactor = 0.5;
im1 = imresize(im1, scalefactor);
im2 = imresize(im2, scalefactor);
im1_original = imresize(im1_original, scalefactor);
im2_original = imresize(im2_original, scalefactor);

%% Detect keypoints and extract descriptors
[f1 d1] = vl_sift(im1);
[f2 d2] = vl_sift(im2);

%% Match features
dist = dist2(double(d1'),double(d2'));

%% Prune features
numFeat = 200;

[~, fcount1] = size(d1);
[~, fcount2] = size(d2);
dist1d = reshape(dist, [], 1); %% turn 2d matrix into 1d
[dsort, dix] = sort(dist1d,'ascend'); %% sort the matrix while retaining indices
topx = [mod(dix(1:numFeat),fcount1), floor(dix(1:numFeat)/fcount1)+1]; %% creat array of indices of matching features

%% Robust transformation estimation
A = ones(3,3);
p = 0.3;          %% threshold value
e = 0.5;        %% probability a point is an outlier
prob = 0.99;    %% target probability that one sample will have all inliers
maxInliers = 0; 

N = log(1-prob)/log(1-(1-e)^3); %% calculate number of iterations
clear aff1 aff2 A bx by;
for iter = 1:N
    s = randperm(numFeat, 3);       %% choose 3 random samples
    for i = 1:3                     %% generate A matrix and b vectors to estimate affine matrix
        A(i,:) = [f1(1,topx(s(i),1)), f1(2,topx(s(i),1)), 1]; 
        bx(i) = f2(1,topx(s(i),2));
        by(i) = f2(2,topx(s(i),2));
    end
    aff1 = A\bx';              %% estimate values for a b tx
    aff2 = A\by';              %% estimate values for c d ty
    aff = reshape([aff1;aff2;[0,0,1]'],[3 3])'; %% create affine transformation matrix from estimated values
    
    % count number of inliers for this affine transformation
    inliers = 0;
    for i = 1:numFeat
        cal = aff*[f1(1:2,topx(i,1));1];    %% calculated coordinates
        act = f2(1:2,topx(i,2));            %% actual coordinates
        if(pdist(reshape([cal(1:2)'; act'],[2,2]),'Euclidean') < p) %% calculate euclidean distance between
            inliers = inliers + 1;
        end
    end
    % check if new max inliers was reached
    if( inliers > maxInliers )
        maxInliers = inliers;
        maxAff = aff;
    end
end
in = ones(maxInliers,2);        % using the optimal affine transformation matrix, create list of inlier indices
j=1;
for i = 1:numFeat
    cal = maxAff*[f1(1:2,topx(i,1));1];
    act = f2(1:2,topx(i,2));
    if(pdist(reshape([cal(1:2)'; act'],[2,2]),'Euclidean') < p)
        in(j,:) = topx(i,:);
        j = j+1;
    end
end

%% Compute optimal transformation
A = [f1(1,in(:,1))' f1(2,in(:,1))' ones(1,maxInliers)'];
bx = f2(1,in(:,2))';
by = f2(2,in(:,2))';

aff1 = inv(A'*A)*A'*bx;
aff2 = inv(A'*A)*A'*by;
aff = reshape([aff1;aff2;[0,0,1]'],[3 3])';

%% Create panorama
tform = maketform('affine', aff');
[im1 xdata_im1 ydata_im1] = imtransform(im1, tform);
im1_original = imtransform(im1_original, tform);

[im2_h, im2_w] = size(im2);
[im1_h, im1_w] = size(im1);

xdata_im2 = [0, im2_w];
ydata_im2 = [0, im2_h];

xdata_im1 = floor(xdata_im1);
ydata_im1 = floor(ydata_im1);

xdata_im1(2) = xdata_im1(1)+im1_w;
ydata_im1(2) = ydata_im1(1)+im1_h;

if(xdata_im1(1) < 0)
    shift = ceil(abs(xdata_im1(1)));
    xdata_im2 = xdata_im2 + shift;
    xdata_im1 = xdata_im1 + shift;
end
if(ydata_im1(1) < 0)
    shift = ceil(abs(ydata_im1(1)));
    ydata_im2 = ydata_im2 + shift;
    ydata_im1 = ydata_im1 + shift;
end

%imfinal = zeros(max(ydata_im1(2), ydata_im2(2)), max(xdata_im1(2),xdata_im2(2)));
imfinal = zeros(max(ydata_im1(2), ydata_im2(2)), max(xdata_im1(2),xdata_im2(2)),3);
%imfinal = overlay(imfinal, im1, xdata_im1, ydata_im1);
%imfinal = overlay(imfinal, im2, xdata_im2, ydata_im2);
for i = 1:3
    imfinal(:,:,i) = overlay(imfinal(:,:,i), im1_original(:,:,i), xdata_im1, ydata_im1);
    imfinal(:,:,i) = overlay(imfinal(:,:,i), im2_original(:,:,i), xdata_im2, ydata_im2);
end
imfinal = uint8(imfinal);
imshow(imfinal);