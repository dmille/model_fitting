a = 2;
b = 4;
r = 6;

fprintf('a = %f\n', a);
fprintf('b = %f\n', b);
fprintf('r = %f\n', r);

x = [1:500]';
y = [1:500]';

x = randn(size(x));
y = randn(size(y));

z = x * a + y * b + r;
z = z + randn(size(y));

A = [x y ones(size(x))];

scatter3(x,y,z);