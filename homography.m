function [ imout ] = homography( im1, im2, scalefactor, numFeat )
%UNTITLED Summary of this function goes here
im1_original = im1;
im2_original = im2;
im1 = single(rgb2gray(im1_original));
im2 = single(rgb2gray(im2_original));

%% Resize images for faster processing
im1 = imresize(im1, scalefactor);
im2 = imresize(im2, scalefactor);
im1_original = imresize(im1_original, scalefactor);
im2_original = imresize(im2_original, scalefactor);

%% Detect keypoints and extract descriptors
[f1 d1] = vl_sift(im1);
[f2 d2] = vl_sift(im2);

%% Match features
dist = dist2(double(d1'),double(d2'));

%% Prune features
[~, fcount1] = size(d1);
[~, fcount2] = size(d2);
dist1d = reshape(dist, [], 1); %% turn 2d matrix into 1d
[dsort, dix] = sort(dist1d,'ascend'); %% sort the matrix while retaining indices
topx = [mod(dix(1:numFeat),fcount1), floor(dix(1:numFeat)/fcount1)+1]; %% creat array of indices of matching features

%% Robust transformation estimation
A = ones(3,3);
p = 0.3;          %% threshold value

maxInliers = 0; 

N = 3000;  %% Set number of iterations
clear aff1 aff2 A bx by;
for iter = 1:N
    A=[];
    s = randperm(numFeat, 4);       %% choose 4 random samples
    for i = 1:4                     %% generate A matrix and b vectors to estimate affine matrix
        x=f1(1,topx(s(i),1));
        y=f1(2,topx(s(i),1));
        xp=f2(1,topx(s(i),2));
        yp=f2(2,topx(s(i),2));
        A = cat(1,A,[x,y,1,0,0,0,-x*xp,-y*xp,-xp]);
        A = cat(1,A,[0,0,0,x,y,1,-x*yp,-y*yp,-yp]);
    end
        [~,~,V] = svd(A);
        X=V(:,end);
        H=reshape(X,[3,3])';
    % count number of inliers for this affine transformation
    inliers = 0;
    for i = 1:numFeat
        cal = H*[f1(1:2,topx(i,1));1];    %% calculated coordinates
        cal(1:2) = cal(1:2)/cal(3);
        act = f2(1:2,topx(i,2));            %% actual coordinates
        if(pdist(reshape([cal(1:2)'; act'],[2,2]),'Euclidean') < p) %% calculate euclidean distance between
            inliers = inliers + 1;
        end
    end
    % check if new max inliers was reached
    if( inliers > maxInliers )
        maxInliers = inliers;
        maxH = H;
    end
end
in = ones(maxInliers,2);        % using the optimal affine transformation matrix, create list of inlier indices
j=1;
for i = 1:numFeat
    cal = maxH*[f1(1:2,topx(i,1));1];
    cal(1:2) = cal(1:2)/cal(3);
    act = f2(1:2,topx(i,2));
    if(pdist(reshape([cal(1:2)'; act'],[2,2]),'Euclidean') < p)
        in(j,:) = topx(i,:);
        j = j+1;
    end
end

%% Compute optimal transformation
A=[];

for i = 1:maxInliers                     %% generate A matrix and b vectors to estimate affine matrix
    x=f1(1,in(i,1));
    y=f1(2,in(i,1));
    xp=f2(1,in(i,2));
    yp=f2(2,in(i,2));
    A = cat(1,A,[x,y,1,0,0,0,-x*xp,-y*xp,-xp]);
    A = cat(1,A,[0,0,0,x,y,1,-x*yp,-y*yp,-yp]);
end

[~,~,V] = svd(A);
X=V(:,end);
H=reshape(X,[3,3])';

%% Create panorama
tform = maketform('projective', H');
[im1 xdata_im1 ydata_im1] = imtransform(im1, tform);
im1_original = imtransform(im1_original, tform);

[im2_h, im2_w] = size(im2);
[im1_h, im1_w] = size(im1);

xdata_im2 = [0, im2_w];
ydata_im2 = [0, im2_h];

xdata_im1 = floor(xdata_im1);
ydata_im1 = floor(ydata_im1);

xdata_im1(2) = xdata_im1(1)+im1_w;
ydata_im1(2) = ydata_im1(1)+im1_h;

if(xdata_im1(1) < 0)
    shift = ceil(abs(xdata_im1(1)));
    xdata_im2 = xdata_im2 + shift;
    xdata_im1 = xdata_im1 + shift;
end
if(ydata_im1(1) < 0)
    shift = ceil(abs(ydata_im1(1)));
    ydata_im2 = ydata_im2 + shift;
    ydata_im1 = ydata_im1 + shift;
end

imfinal = zeros(max(ydata_im1(2), ydata_im2(2)), max(xdata_im1(2),xdata_im2(2)),3);

for i = 1:3
    imfinal(:,:,i) = overlay(imfinal(:,:,i), im1_original(:,:,i), xdata_im1, ydata_im1);
    imfinal(:,:,i) = overlay(imfinal(:,:,i), im2_original(:,:,i), xdata_im2, ydata_im2);
end
imfinal = uint8(imfinal);
imout = imfinal;

end

