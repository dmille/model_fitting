function [ master ] = overlay( master, im, xdata, ydata )
%OVERLAY Summary of this function goes here
%   Detailed explanation goes here
[im_w, im_h] = size(im);
tx = floor(xdata(1));
ty = floor(ydata(1));
for i = 1:im_w
    for j = 1:im_h
        if(master(i+ty,j+tx) < im(i, j))
            master(i+ty,j+tx) = im(i,j);   
        end
    end    
end

end

